# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import os
import sys
from importlib import import_module

import psutil
from PyQt4 import QtGui, QtCore


class EmittingStream(QtCore.QObject):
    textWritten = QtCore.pyqtSignal(str)

    def write(self, text):
        self.textWritten.emit(str(text))

    def flush(self):
        pass


class PageRecap(QtGui.QWizardPage):
    """ Fourth QWizard Page for recapitulation"""

    def __init__(self, parent=None):

        super(PageRecap, self).__init__(parent)
        self.setTitle("Check the configuration and run.")
        self.setSubTitle("Review the simulation options and paramaters, and run simulation.")
        # self.setButtonText(QtGui.QWizard.NextButton, "Simulate")

        # Création des labels et lignes pour rentrer le texte
        self.lbl_username = QtGui.QLabel("Username: ", self)  # label de texte pour le user name
        self.lbl_model = QtGui.QLabel("Selected model: ", self)  # label de texte pour le user name
        self.lbl_purp = QtGui.QLabel("Purpose: ", self)  # label de texte pour le user name
        self.lbl_dir = QtGui.QLabel("Simulation directory: ", self)  # label de texte pour le user name
        self.lbl_cor = QtGui.QLabel("Cores to use: ", self)  # label de texte pour le user name
        # self.lbl_out = QtGui.QLabel("Outputs: ", self) #label de texte pour le user name
        self.lbl_batch = QtGui.QLabel("Batch mode:", self)

        self.val_username = QtGui.QLabel("", self)  # label de texte pour le user name
        self.val_model = QtGui.QLabel("", self)  # label de texte pour le modèle
        self.val_purp = QtGui.QLabel("", self)  # label de texte pour le user name
        self.val_dir = QtGui.QLabel("", self)  # label de texte pour le user name
        self.val_cor = QtGui.QLabel("", self)  # label de texte pour le user name
        # self.val_out = QtGui.QLabel("", self) #label de texte pour le user name
        # Spinbox
        self.spin_cores = QtGui.QSpinBox(self)
        self.spin_cores.setMinimum(1)
        self.spin_cores.setMaximum(psutil.cpu_count())
        self.cores_box = QtGui.QHBoxLayout()
        self.cores_box.addWidget(self.spin_cores)
        self.cores_box.addWidget(self.val_cor)
        # Création des combobox
        self.cb_batch = QtGui.QCheckBox('Enable')
        self.simulated = False

        flayout = QtGui.QFormLayout()
        flayout.addRow(self.lbl_username, self.val_username)
        flayout.addRow(self.lbl_model, self.val_model)
        flayout.addRow(self.lbl_purp, self.val_purp)
        flayout.addRow(self.lbl_dir, self.val_dir)
        flayout.addRow(self.lbl_cor, self.cores_box)
        flayout.addRow(self.lbl_batch, self.cb_batch)

        self.textEdit = QtGui.QTextEdit()
        self.progress = WaitBar(self)
        VBOX = QtGui.QVBoxLayout()
        VBOX.addLayout(flayout)
        VBOX.addWidget(self.progress.progressBar)
        VBOX.addWidget(self.textEdit)
        sys.stdout = EmittingStream()
        sys.stdout.textWritten.connect(self.normal_output_written)
        sys.stderr = EmittingStream()
        sys.stderr.textWritten.connect(self.normal_output_written)
        #sys.stdout = EmittingStream(textWritten=self.normal_output_written)
        self.setLayout(VBOX)

    def initializePage(self):
        self.wizard().setButtonText(QtGui.QWizard.CustomButton1, "Simulate")
        self.wizard().setOption(QtGui.QWizard.HaveCustomButton1, True)
        self.wizard().customButtonClicked.connect(self.progress.onStart)
        self.val_username.setText(self.wizard().data['username'])  # label de texte pour le user name
        self.val_model.setText(self.wizard().data['model'])  # label de texte pour le modèle
        self.val_purp.setText(self.wizard().data['purpose'])  # label de texte pour le user name
        self.val_dir.setText(self.wizard().data['directory'])  # label de texte pour le user name
        self.val_cor.setText("/ {} cores.".format(psutil.cpu_count()))  # label de texte pour le user name

    def normal_output_written(self, text):
        """Append text to the QTextEdit."""
        # Maybe QTextEdit.append() works as well, but this is how I do it:
        cursor = self.textEdit.textCursor()
        cursor.movePosition(QtGui.QTextCursor.End)
        cursor.insertText(text)
        self.textEdit.setTextCursor(cursor)
        self.textEdit.ensureCursorVisible()

    def __del__(self):
        # Restore sys.stdout
        sys.stdout = sys.__stdout__
        sys.stderr = sys.__stderr__

    def validatePage(self):
        if self.simulated:
            self.wizard().setOption(QtGui.QWizard.HaveCustomButton1, False)
            sys.stdout = sys.__stdout__
            sys.stderr = sys.__stderr__
            return True
        else:
            QtGui.QMessageBox.information(None, "Are you sure?", "No simulation has been run.")
            return False
            # return False


class WaitBar(QtGui.QWidget):
    def __init__(self, parent=None):
        super(WaitBar, self).__init__(parent)
        self.wizpage = parent
        self.batch = parent.cb_batch.isChecked()
        # Create a progress bar and a button and add them to the main layout
        self.progressBar = QtGui.QProgressBar(self)
        self.progressBar.setRange(0, 1)

        self.myLongTask = SimuThread(parent)
        self.myLongTask.taskFinished.connect(self.onFinished)

    def onStart(self):
        self.progressBar.setRange(0, 0)
        self.myLongTask.start()

    def onFinished(self):
        # Stop the pulsation
        self.progressBar.setRange(0, 1)
        self.wizpage.simulated = True


class SimuThread(QtCore.QThread):
    taskFinished = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        self.parent = parent
        self.wizard = parent.wizard
        super(SimuThread, self).__init__()

    def run(self):
        smdl = import_module(self.wizard().data['model'])
        mdl = smdl.Manager(self.wizard().data['json'], str(self.wizard().data['directory']))
        mdl.compute()
        mdl.finish(batch=self.parent.cb_batch.isChecked())
        self.wizard().data["simdir"] = mdl.subdir
        self.wizard().data["report"] = os.path.join(mdl.subdir, "report.html")
        self.taskFinished.emit()
