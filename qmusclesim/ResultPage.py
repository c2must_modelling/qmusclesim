# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import glob
import os

import h5py
from PyQt4 import QtGui
from PyQt4.QtCore import QUrl
from PyQt4.QtWebKit import QWebView

from .emg_explorer import EMGExplorer
from .emg_viewer import EMGViewer
from .surf_viewer import SurfViewer


class PageResult(QtGui.QWizardPage):
    """ Fourth QWizard Page for recapitulation"""

    def __init__(self, parent=None):

        super(PageResult, self).__init__(parent)
        self.setTitle("Simulation report and results")
        self.setSubTitle("View simulation report and signals.")
        self.report = QWebView()

        self.eviewer = EMGViewer()
        self.sviewer = SurfViewer()
        self.explorer = EMGExplorer()

        self.btn_surf = QtGui.QPushButton("View surface(s)")
        self.btn_surf.clicked.connect(self.sviewer.showMaximized)
        self.btn_emg = QtGui.QPushButton("View EMG signals")
        self.btn_emg.clicked.connect(self.eviewer.showMaximized)
        self.btn_expl = QtGui.QPushButton("Explore EMG&Surfaces")
        self.btn_expl.clicked.connect(self.explorer.showMaximized)
        self.results = None

        HBOX = QtGui.QHBoxLayout()
        HBOX.addWidget(self.btn_surf)
        HBOX.addWidget(self.btn_emg)
        HBOX.addWidget(self.btn_expl)

        VBOX = QtGui.QVBoxLayout()
        VBOX.addWidget(self.report, 1)
        VBOX.addLayout(HBOX)
        self.setLayout(VBOX)

    def loadreport(self):
        self.report.load(QUrl(self.wizard().data["report"]))

    def initializePage(self):
        self.loadreport()
        # try:
        #    smdl = import_module(".".join([self.wizard().data['model'], "surf_viewer"]))
        #    self.sviewer = smdl.SurfViewer()
        # except ImportError:

        self.results = glob.glob(os.path.join(self.wizard().data["simdir"], '*.hdf5'))[0]
        with h5py.File(self.results, "r") as f:
            if not ("myometrium" in f.keys() or "surface" in f.keys() or "Skin" in f.keys):
                self.btn_surf.setEnabled(False)
            if "EMG" not in f.keys():
                self.btn_emg.setEnabled(False)
            if "EMG" not in f.keys() or not ("myometrium" in f.keys() or "surface" in f.keys() or "Skin" in f.keys):
                self.btn_expl.setEnable(False)

    def validatePage(self):
        self.wizard().close()
        return True
