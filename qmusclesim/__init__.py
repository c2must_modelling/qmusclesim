# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import sys

from PyQt4 import QtGui

from . import ConfigPage
from . import InitPage
from . import ParamPage
from . import RecapPage
from . import ResultPage


class IHM(QtGui.QWizard):
    # Nombre d'interfaces
    (InitPage, PageParam, RecapPage, ResultPage) = range(4)  # ConfigPage

    def __init__(self, parent=None):
        # Création du dictionnaire où l'on va stocker les données
        self.data = dict()
        self.data['json'] = ""
        self.data['username'] = ""
        self.data['outs'] = dict()
        self.data['report'] = ""

        super(IHM, self).__init__(parent)

        # On ajoute les pages
        self.setPage(self.InitPage, InitPage.PageInit())
        self.setPage(self.PageParam, ParamPage.PageParam())
        self.setPage(self.RecapPage, RecapPage.PageRecap())
        self.setPage(self.ResultPage, ResultPage.PageResult())

        # Titre et configuration de la fenetre
        self.setWindowTitle("Qt Muscle Simulation Manager")


def main():
    app = QtGui.QApplication(sys.argv)
    wizard = IHM()
    wizard.showMaximized()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
