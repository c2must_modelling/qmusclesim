# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

try:
    import uemg2
except ImportError:
    uemg2 = None

try:
    import uemg_squared
except ImportError:
    uemg_squared = None

try:
    import semgc
except ImportError:
    semgc = None

from PyQt4 import QtGui
from getpass import getuser
import os
import json
from pkg_resources import resource_filename


class PageInit(QtGui.QWizardPage):
    def __init__(self, parent=None):
        super(PageInit, self).__init__(parent)
        self.setTitle("General configuration")
        self.setSubTitle("Set the general informations for a simulation.")
        # Création d'une combobox pour choisir le modèle
        models = [uemg2, uemg_squared, semgc]
        availmdls = []

        self.model_combobox = QtGui.QComboBox(self)

        for mdl in models:
            if mdl is not None:
                availmdls.append(mdl.__name__)
                self.model_combobox.addItem(mdl.__name__)
        if len(availmdls) == 0:
            raise ImportError("No models could be imported, check installation.")

        self.lbl_combobox = QtGui.QLabel("Model:", self)  # label de texte pour le modèle
        # Création des labels et lignes pour rentrer le texte

        self.lbl_username = QtGui.QLabel("User name:", self)  # label de texte pour le user name
        self.ligne_username = QtGui.QLineEdit(self)  # Création d'une ligne de texte pour le user name
        self.lbl_directory = QtGui.QLabel("Working directory:", self)  # label de texte pour le diectory

        btn_browser = QtGui.QPushButton('...', self)  # Bouton pour browser le directory
        btn_browser.clicked.connect(self.select_workdir)
        btn_browser.setToolTip('Browser')

        self.ligne_directory = QtGui.QLineEdit(self)  # Création d'une ligne de texte pour le directory

        self.lbl_purpose = QtGui.QLabel("Purpose", self)  # label de texte pour le user name

        self.ligne_purpose = QtGui.QTextEdit(self)  # Création d'un cadre de texte pour le user name

        #
        browshbox = QtGui.QHBoxLayout()
        browshbox.addWidget(btn_browser)
        browshbox.addWidget(self.ligne_directory)
        layoutf = QtGui.QFormLayout()
        layoutf.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)
        layoutf.addRow(self.lbl_combobox, self.model_combobox)
        layoutf.addRow(self.lbl_username, self.ligne_username)
        layoutf.addRow(self.lbl_directory, browshbox)
        layoutf.addRow(self.lbl_purpose, self.ligne_purpose)

        self.setLayout(layoutf)

    def initializePage(self):
        self.ligne_username.setText(getuser())

    def select_workdir(self):
        dirname = QtGui.QFileDialog.getExistingDirectory(self)  # Directory line
        self.ligne_directory.setText(str(dirname))  # On met le Filename dans la ligne dédiée au directory

    def validatePage(self):
        # Vérifications des rentrées
        if self.ligne_username.displayText() == "":
            self.showDialog_username()
            return False
        else:
            self.wizard().data['username'] = self.ligne_username.displayText()

        if self.ligne_directory.displayText() == "" or not os.path.isdir(self.ligne_directory.displayText()):
            self.showDialog_directory()
            return False
        else:
            self.wizard().data['directory'] = self.ligne_directory.displayText()

        if self.ligne_purpose.toPlainText() == "":
            self.showDialog_purpose()
            return False
        else:
            self.wizard().data['purpose'] = self.ligne_purpose.toPlainText()

        # Stockage et importation du modèle choisi
        self.wizard().data['model'] = str(self.model_combobox.currentText())

        with open(resource_filename(self.wizard().data['model'], 'data/UnitsDictionary.json'), 'r') as f_units:
            self.wizard().units = json.load(f_units)
            #
        return True

    def showDialog_directory(self):

        text = QtGui.QMessageBox(self)
        text.open()
        text.setWindowTitle("Something's missing!")
        text.setGeometry(650, 400, 450, 1000)
        text.setText("Please select a valid working directory.")

    def showDialog_username(self):

        text = QtGui.QMessageBox(self)
        text.open()
        text.setWindowTitle("Something's missing!")
        text.setGeometry(650, 400, 250, 1000)
        text.setText("Please enter your name.")

    def showDialog_purpose(self):

        text = QtGui.QMessageBox(self)
        text.open()
        text.setWindowTitle("Something's missing!")
        text.setGeometry(650, 350, 250, 1000)
        text.setText("Please enter the purpose of the simulation.")
