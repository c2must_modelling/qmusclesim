# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import psutil
from PyQt4 import QtGui


class PageConfig(QtGui.QWizardPage):
    """ Third QWizard Page for configuration"""
    def __init__(self, parent=None):

        super(PageConfig, self).__init__(parent)
        self.setTitle("Configure simulation performance")
        self.setSubTitle("Set computation option and define outputs.")
        #Labels de texte
        self.lbl_cores = QtGui.QLabel("Available cores: " + str(psutil.cpu_count()), self)
        self.lbl_mem = QtGui.QLabel("Available memory: " + str(psutil.virtual_memory()[0]/(1024**3))[:5] + " Gb", self)
        self.lbl_use = QtGui.QLabel("Cores to use:", self)

        #Spinbox
        self.spin_cores = QtGui.QSpinBox(self)
        self.spin_cores.setMinimum(1)
        self.spin_cores.setMaximum(psutil.cpu_count())


        self.lbl_out = QtGui.QLabel("Outputs:", self)

        #Création des combobox
        self.out_checks = QtGui.QGroupBox()
        self.cb_sig = QtGui.QCheckBox('Signals', self.out_checks)
        self.cb_ima = QtGui.QCheckBox('Images', self.out_checks)
        self.cb_vid = QtGui.QCheckBox('Videos', self.out_checks)

        VBOX_outputs = QtGui.QVBoxLayout()
        VBOX_outputs.addWidget(self.cb_sig)
        VBOX_outputs.addWidget(self.cb_ima)
        VBOX_outputs.addWidget(self.cb_vid)
        self.out_checks.setLayout(VBOX_outputs)

        flayout = QtGui.QFormLayout()
        flayout.addRow(self.lbl_cores)
        flayout.addRow(self.lbl_mem)
        flayout.addRow(self.lbl_use, self.spin_cores)
        flayout.addRow(self.lbl_out, self.out_checks)

        self.setLayout(flayout)

    def validatePage(self):
        self.wizard().data['cores'] = self.spin_cores.text()
        self.wizard().data['outs'] = dict()
        self.wizard().data['outs']["Ima"] = ""
        self.wizard().data['outs']["Sig"] = ""
        self.wizard().data['outs']["Vid"] = ""
        checked = 0
        for checkbox in [child for child in self.out_checks.children() if isinstance(child, QtGui.QCheckBox)]:
            if checkbox.isChecked():
                checked += 1

        if checked == 0:
            QtGui.QMessageBox.warning(None, "Are you sure?",
                                      "No outputs were selected, please check your configuration.")
            return False

        if self.cb_ima.isChecked():
            self.wizard().data['outs']["Ima"] += 'Images '
        if self.cb_sig.isChecked():
            self.wizard().data['outs']["Sig"] += 'Signals '
        if self.cb_vid.isChecked():
            self.wizard().data['outs']["Vid"] += 'Videos'
        #self.wizard().page(self.wizard().RecapPage).racapShow()
        return True

    '''def cleanupPage(self):
        self.wizard().data['xml'] = ""'''