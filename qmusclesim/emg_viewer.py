# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import itertools
import os
import sys

import h5py
import matplotlib
import matplotlib.pyplot
import numpy
import pyqtgraph as pg
from PyQt4 import QtGui
from pkg_resources import resource_filename
from pyqtgraph import exporters

matplotlib.pyplot.style.use('bmh')


class EMGViewer(QtGui.QMainWindow):
    def __init__(self):
        super(EMGViewer, self).__init__()
        self.filename = None
        self.graph = None
        self.grids = []
        exitAction = QtGui.QAction(QtGui.QIcon(resource_filename(self.__module__.split('.')[0], 'icons/exit.png')),
                                   'Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setToolTip('Exit application')
        exitAction.triggered.connect(self.close)

        loadAction = QtGui.QAction(QtGui.QIcon(resource_filename(self.__module__.split('.')[0], 'icons/open.png')),
                                   'Load', self)
        loadAction.setShortcut('Ctrl+O')
        loadAction.setToolTip('load application')
        loadAction.triggered.connect(self.load)

        self.minimapAction = QtGui.QAction(
            QtGui.QIcon(resource_filename(self.__module__.split('.')[0], 'icons/minimap.png')), 'Minimap', self)
        self.minimapAction.setShortcut('Ctrl+M')
        self.minimapAction.setToolTip('Open Minimap')
        self.minimapAction.triggered.connect(self.minimap)
        self.minimapAction.setEnabled(False)

        self.photoAction = QtGui.QAction(
            QtGui.QIcon(resource_filename(self.__module__.split('.')[0], 'icons/photo.png')), 'ScreenShot', self)
        self.photoAction.setToolTip("Screenshot simulation")
        self.photoAction.triggered.connect(self.screenshot)
        self.photoAction.setEnabled(False)

        aboutAction = QtGui.QAction('About', self)
        aboutAction.setToolTip("About qtviewer")
        aboutAction.triggered.connect(self.about)

        menubar = self.menuBar()
        menubar.setNativeMenuBar(False)
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(loadAction)
        fileMenu.addAction(exitAction)
        toolMenu = menubar.addMenu('&Tools')
        toolMenu.addAction(self.minimapAction)
        toolMenu.addAction(self.photoAction)
        menubar.addAction(aboutAction)

        toolbar = self.addToolBar('Exit')
        toolbar.addAction(loadAction)
        toolbar.addAction(exitAction)
        toolbar.addSeparator()
        toolbar.addWidget(QtGui.QLabel("Grid:"))
        self.combo_grid = QtGui.QComboBox()
        self.combo_grid.currentIndexChanged.connect(self.plot)
        self.combo_grid.setEnabled(False)
        toolbar.addWidget(self.combo_grid)
        toolbar.addAction(self.minimapAction)
        toolbar.addSeparator()
        self.fftbutton = QtGui.QPushButton("FFT")
        self.fftbutton.setCheckable(True)
        self.fftbutton.clicked[bool].connect(self.set_fft_mode)
        self.fftbutton.setEnabled(False)
        toolbar.addWidget(self.fftbutton)
        toolbar.addSeparator()
        toolbar.addAction(self.photoAction)
        self.setWindowTitle('EMG QtViewer v0.1')
        self.figmap = None
        self.plots = []

    def about(self):
        QtGui.QMessageBox.about(None, "About QtEMGViewer", "Simulated EMG Signals viewer.")

    def screenshot(self, filename=None):
        if not filename:
            filename = str(QtGui.QFileDialog.getSaveFileName(self, "Save file", "", "PNG (*.png);; SVG (*.svg)"))
        exporters.ImageExporter(self.graph.scene()).export(filename)

    def load(self):
        fname = str(QtGui.QFileDialog.getOpenFileName(self, 'Open HDF5 file',
                                                      '', "HDF5 (*.hdf5)"))
        if fname:
            self.workdir, self.filename = os.path.split(fname)
            with h5py.File(fname, "r") as f:
                self.time = numpy.array(f["/time"])*1e-3
                try:
                    emg = f["/EMG"]
                except KeyError:
                    emg = f
                else:
                    emg = f["/EMG"]
                grid_names = [group for group in emg.keys() if group.startswith("Grid")]
                for name in grid_names:
                    grid = dict(emg[name].attrs)
                    grid["id"] = name.split("_")[-1]
                    grid["signals"] = numpy.array(emg[name]["Monopolar"])
                    self.grids.append(grid)

        self.nbgrids = len(self.grids)
        for id in range(self.nbgrids):
            self.combo_grid.addItem(str(id))
        self.combo_grid.setEnabled(True)
        self.plot()
        self.photoAction.setEnabled(True)
        self.fftbutton.setEnabled(True)
        self.minimapAction.setEnabled(True)

    def plot(self):
        if self.grids:
            self.fftbutton.setChecked(False)
            pg.setConfigOption('background', (238, 238, 238))
            pg.setConfigOption('foreground', 'k')
            grid_id = self.combo_grid.currentIndex()
            if self.figmap:
                matplotlib.pyplot.close(self.figmap)
            sigs = self.grids[grid_id]["signals"]
            self.sig_min = sigs.min()
            self.sig_max = sigs.max()
            nbelec = sigs.shape[0]
            self.nx = self.grids[grid_id]["centers"].shape[-2]
            self.ny = self.grids[grid_id]["centers"].shape[-1]
            idlist = numpy.arange(nbelec, dtype='int32').reshape((self.nx, self.ny))[:, ::-1].T
            xyid = [xy for xy in itertools.product(range(self.nx), range(self.ny))]
            self.graph = pg.GraphicsLayoutWidget()
            pen = pg.mkPen(color=(52, 138, 189), width=2)
            self.setCentralWidget(self.graph)
            first = True
            for xy in xyid:
                pitem = self.graph.addPlot(row=xy[0], col=xy[1])
                pitem.setMouseEnabled(x=True, y=False)
                pitem.setLimits(xMin=0, xMax=self.time.max())
                self.plots.append(pitem.plot(self.time, sigs[idlist[xy]], pen=pen))
                pitem.setYRange(self.sig_min, self.sig_max)
                pitem.showGrid(x=True)
                pitem.setMenuEnabled(False)
                if not xy[1] % self.ny:
                    pitem.setLabel('left', "EMG", "mV")
                if xy[0] % self.nx == self.nx - 1:
                    pitem.setLabel('bottom', "time", "s")
                txt = pg.TextItem(text='{}'.format(idlist[xy]), color=(0, 0, 0), anchor=(1, 0),
                                  fill=pg.mkBrush(255, 255, 255, 190), border=pg.mkPen((0, 0, 0)))

                pitem.addItem(txt)
                txt.setPos(self.time.max(), sigs.max())
                if first:
                    self.firstpitem = pitem
                    first = False
                else:
                    pitem.setXLink(self.firstpitem)

    def minimap(self, grid_id=None):
        if self.grids:
            if not grid_id:
                grid_id = self.combo_grid.currentIndex()
            shape = self.grids[grid_id]["tissue_shape"]
            centers_x = self.grids[grid_id]["centers"][1]
            centers_y = self.grids[grid_id]["centers"][0]
            background = numpy.zeros(shape)
            self.figmap = matplotlib.pyplot.figure("minimap", tight_layout=True)
            self.figmap.suptitle("Electrode positions",
                                 bbox=dict(boxstyle='round', facecolor='white', alpha=0.9, pad=0.5))
            cmaplist = matplotlib.colors.ListedColormap(
                [col['color'] for col in matplotlib.pyplot.rcParams['axes.prop_cycle']][:2])
            matplotlib.pyplot.imshow(background, origin='lower', cmap=cmaplist)
            for idx in range(centers_x.size):
                matplotlib.pyplot.text(centers_x.flatten()[idx], centers_y.flatten()[idx], "{}".format(idx),
                                       horizontalalignment='center', verticalalignment='center',
                                       bbox=dict(boxstyle='circle', facecolor='white', alpha=0.5, pad=0.5))
            self.figmap.show()

    def set_fft_mode(self, pressed):
        if pressed:
            xyid = [xy for xy in itertools.product(range(self.nx), range(self.ny))]
            for xid, yid in xyid:
                p = self.graph.getItem(xid, yid)
                p.curves[0].setFftMode(True)
                p.enableAutoRange(True)
                if not yid % self.ny:
                    p.setLabel('left', "Power", "")
                if xid % self.nx == self.nx - 1:
                    p.setLabel('bottom', "Frequency", "Hz")
        else:
            self.plot()


def main():
    app = QtGui.QApplication(sys.argv)
    ex = EMGViewer()
    ex.showMaximized()
    ex.raise_()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
