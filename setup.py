from setuptools import setup, find_packages

setup(name='qmusclesim',
      version='0.5',
      description='Muscle model simulation manager.',
      url='https://gitlab.utc.fr/jlaforet/qmusclesim',
      author='Jeremy Laforet',
      author_email='jlaforet@utc.fr',
      license='GPLv3',
      packages=find_packages(),
      entry_points={'gui_scripts': ["qmusclesim = qmusclesim.__init__:main",
                                    "qemg_viewer = qmusclesim.emg_viewer:main",
                                    "qsurf_viewer = qmusclesim.surf_viewer:main",
                                    "qemg_explorer = qmusclesim.emg_explorer:main"]},
      package_data={'qmusclesim': ['icons/*.png']},
      install_requires=['h5py', 'jsonschema', 'psutil', 'scipy', 'numpy', 'pyqtgraph',
                        'matplotlib']

      )
