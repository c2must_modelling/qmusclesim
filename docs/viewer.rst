
Use the Viewer
===============

Open the Viewer
---------------

On windows
From a cmd windows

.. code-block:: python

    C:\Users>python #launch python
    >>>import uemg_squared.surf_viewer #import the viewer
    >>>uemg_squared.surf_viewer.main() #launch the viewer


The interface
-------------
This interface example with a simulation of a 2D surface

.. image:: img_viewer/global.png
    :width: 800pt

This interface example with a simulation of a 3d volume

.. image:: img_viewer/global3d.png
    :width: 800pt

The icons
---------

+----------------------------------+---------------------------------------------------------------------------------+
| .. image:: img_viewer/open.png   |  load a hdf5 file.                                                              |
|    :width: 30pt                  |  Can also be found at File/Load.                                                |
+----------------------------------+---------------------------------------------------------------------------------+
| .. image:: img_viewer/exit.png   |  Exit the program.                                                              |
|     :width: 30pt                 |  Can also be found at File/Exit.                                                |
+----------------------------------+---------------------------------------------------------------------------------+
| .. image:: img_viewer/add.png    |add a result from the hdf5 file. It can be electrical, mechanical or deformation.|
|     :width: 30pt                 |                                                                                 |
|                                  |Can also be found at View/AddView.                                               |
+----------------------------------+---------------------------------------------------------------------------------+
| .. image:: img_viewer/dot.png    |  Modify the size of each dot in the scatter plot.                               |
|     :width: 30pt                 |                                                                                 |
+----------------------------------+---------------------------------------------------------------------------------+
| .. image:: img_viewer/edit.png   |  Modify parameters of each selected view.                                       |
|     :width: 30pt                 |  Can also be found at View/ModView.                                             |
+----------------------------------+---------------------------------------------------------------------------------+
| .. image:: img_viewer/remove.png |  Remove the selected view.                                                      |
|     :width: 30pt                 |  Can also be found at View/RemView.                                             |
+----------------------------------+---------------------------------------------------------------------------------+
|.. image:: img_viewer/play.png    |  Launch the animation according to the time on the slider.                      |
|    :width: 30pt                  |  Can also be found at Simulation/Play.                                          |
+----------------------------------+---------------------------------------------------------------------------------+
|.. image:: img_viewer/pause.png   |  Pause the animation.                                                           |
|    :width: 30pt                  |  Can also be found at Simulation/Pause.                                         |
+----------------------------------+---------------------------------------------------------------------------------+
|.. image:: img_viewer/photo.png   |  Take a screensot of the result at the current time.                            |
|    :width: 30pt                  |  Can also be found at Simulation/ScreenShot.                                    |
+----------------------------------+---------------------------------------------------------------------------------+
|.. image:: img_viewer/video.png   |  Make a video of the result from the current time.                              |
|    :width: 30pt                  |  Can also be found at Simulation/Video.                                         |
+----------------------------------+---------------------------------------------------------------------------------+


The sub windows
---------------

Add a View
~~~~~~~~~~

Once clicked on View/AddView (or add icon) or View/ModView (or edit icon)

.. image:: img_viewer/addview.png
    :width: 300pt

Here, we can select, for each new or already made view, the parameter of the result representation.

* Variable:
    - select Vm to represent the electrical transmebrane voltage of each cell
    - select Force to represent the mechanical force generation of each cell
    - select blank to represent positions of each cell

* Mode:
    - select ScatterPlot to have a scatter plot view of the result (see second interface image in `The interface`_ section
    - select Textured to have a textured view of the result (see first interface image in `The interface`_ section

* Deformation:
    - select Active to see the deformation of the tissue
    - select Inactive to fix the position of each cell

* Colormap:
    - choose your favorite color map

Modify dot sizes
~~~~~~~~~~~~~~~~

Once clicked on setScatterSize icon

.. image:: img_viewer/dotsize.png
    :width: 150pt

Modify the size of the scatter dots

Save Image or Video
~~~~~~~~~~~~~~~~~~~

Once clicked on photo icon or Video icon

.. image:: img_viewer/save.png
    :width: 300pt

* Screenshot:
    - enter a file name and select a place to save the image

* Video:
    - enter a file name with one of the proposed extension (.gif, .mp4, .webm or .ogv) and select a place to save the image