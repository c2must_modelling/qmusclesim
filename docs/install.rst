Installation Instructions
=========================

Dependencies
------------

Mandatory:

* numpy
* scipy
* astropy
* jsonschema
* h5py
* psutils


For graphical outputs:

* matplotlib
* pyqt4
* pyqtgraph
* moviepy

For optional progress bar during simulations:

* progressbar

All depencies are available on `pypi <https://pypi.python.org/>`_ and can be installed with `pip`, they should be automatically installed if
necessary during the installation of the `uemg_squared` wheel.

Exception for `pyqt4` which requires manual installation either:

* using the conda package `pyqt`
* using the package manager of your system
* directly from `Riverbank Computing <https://www.riverbankcomputing.com/software/pyqt/download>`_ (for windows)

`pyqt4` is required by pyqtgraph and for the results viewer gui.

Installing from wheel package (recommended):
--------------------------------------------

.. code-block:: shell

    pip install uemg_squared-0.1-py2.py3-none-any.whl



Installing from sources
-----------------------

.. code-block:: shell

    tar xvzf uemg_squared-0.1.tar.gz
    cd uemg_squared-0.1
    python setup.py install