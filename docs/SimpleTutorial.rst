
Simple Tutorial
===============

Import the module uemg\_squared
-------------------------------

The uemg\_squared module should previously be installed. Refer to
:doc:`install` for its installation

.. code:: python

    import uemg_squared

Build a Simulaion json file
---------------------------

If you already have a simulation json file, you can pass this step. In
order to run a simulation, a simulation json file need to be created. It
contains every information about the simulation. In particular
information about the models used (electrical, mechanical or/and
deformation), about the parameters of each model and the stimulation
pattern occurring over the simulation. Bottom, a simple json file which
uses the electrical (Red3) model and the mechanical (Hai murphy) model
on a 2D tissue (125x125 cells). Because the parameters of each model
have not been specified, the default parameters are used. Two pacemakers
area have been set for this simulation: one at the cell position [2,2]
at 2 ms and one at the cell position [75,75] at 600 ms. Each of them are
500 ms long.

.. code:: python

    %%writefile test.json
    {
      "Purpose": "Analytical parameters for electrical potential distribution",
      "Timestamp": "08/12/2014 15:21:01",
      "Muscle": {
        "Kind": "uterine",
        "parameters": [
        ],
        "Models": [
          {
            "name": "Tissue grid",
            "class": "tissular.TissueGrid",
            "parameters": [
                            {"name": "shape","unit": "","value": [125,125 ]},
              {"name": "tmax","unit": "ms","value": 2e3},
              {"name": "stim_list","unit":"vector","value":[{"amp":0.3,"delay":2,"duration":500,"loc":[25,25],"size":[2,2]},
                                                            {"amp": 0.3,"delay": 600,"duration": 500,"loc": [75,75],"size": [2,2]}
                                                           ]
              }
            ]
          },
          {
            "name": "Red3",
            "class": "cellular.Red3",
            "parameters": [
            ]
          },
          {
            "name": "H&M",
            "class": "cellular.HaiMurphy",
            "parameters": [
            ]
          }
        ]
      }
    }
    


.. parsed-literal::

    Writing test.json
    

Create a new class according to the json file
---------------------------------------------

From the json file created previously, we can create a new class be
using the command uemg\_squared.Manager('File.json').

.. code:: python

    m = uemg_squared.Manager('test.json')

Execute the simulation
----------------------

To execut the simulation, use the compute() method of the uemg\_squared
class. The simulation will be done according to the json file.

.. code:: python

    m.compute() 

Save data result and create a report of the simulation
------------------------------------------------------

In order to save the result of the simulation and create a report with a
summary of the simulation and a quick plot of the results, use the
finish() method of the uemg\_squared class. It creates automatically a
hdf5 file which contains all the result variable of the simulation
(Result\_Date\_Timecode.hdf5, ex: Results\_20160308\_141352.hdf5). It
also creates a report in a html format (report.html)

.. code:: python

    m.finish()

It creates automatically a hdf5 file which contains all the result
variable of the simulation (Result\_Date\_Timecode.hdf5, ex:
Results\_20160308\_141352.hdf5). It also creates a report in a html
format (report.html)

.. code:: python

    run ../uemg_squared/surf_viewer
    

