
Json example
============

Short Json: Electrical
----------------------

Below, a short version of a .json file simulation where all electrical
model parameters are automatically chosen to be standard. Only one
stimulation has been set at the cell position [25,25] at 2 ms for 500
ms.

.. code:: python

    # %load Json/ShortJson_electrical.json
    {
      "Purpose": "Analytical parameters for electrical potential distribution",
      "Timestamp": "08/12/2014 15:21:01",
      "Muscle": {
        "Kind": "uterine",
        "parameters": [
        ],
        "Models": [
          {
            "name": "Tissue grid",
            "class": "tissular.TissueGrid",
            "parameters": [
                            {"name": "shape","unit": "","value": [125,125 ]},
              {"name": "tmax","unit": "ms","value": 2e3},
              {"name": "stim_list","unit":"vector","value":[{"amp":0.3,"delay":2,"duration":500,"loc":[25,25],"size":[2,2]}}
                                                           ]
              }
            ]
          },
          {
            "name": "Red3",
            "class": "cellular.Red3",
            "parameters": [
            ]
          }
        ]
      }
    }

Short Json: Electrical and Mechanical
-------------------------------------

Below, a short version of a .json file simulation where all electrical
and mechanical model parameters are automatically chosen to be standard.
Only one stimulation has been set at the cell position [25,25] at 2 ms
for 500 ms.

.. code:: python

    # %load Json/ShortJson_electrical_mechanical.json
    {
      "Purpose": "Analytical parameters for electrical potential distribution",
      "Timestamp": "08/12/2014 15:21:01",
      "Muscle": {
        "Kind": "uterine",
        "parameters": [
        ],
        "Models": [
          {
            "name": "Tissue grid",
            "class": "tissular.TissueGrid",
            "parameters": [
                            {"name": "shape","unit": "","value": [125,125 ]},
              {"name": "tmax","unit": "ms","value": 2e3},
              {"name": "stim_list","unit":"vector","value":[{"amp":0.3,"delay":2,"duration":500,"loc":[25,25],"size":[2,2]}}
                                                           ]
              }
            ]
          },
          {
            "name": "Red3",
            "class": "cellular.Red3",
            "parameters": [
            ]
          },
          {
            "name": "H&M",
            "class": "cellular.HaiMurphy",
            "parameters": [
            ]
          }
        ]
      }
    }

Short Json: Electrical, Mechanical and Deformation
--------------------------------------------------

Below, a short version of a .json file simulation where all electrical,
mechanical and deformation model parameters are automatically chosen to
be standard. Only one stimulation has been set at the cell position
[25,25] at 2 ms for 500 ms.

.. code:: python

    # %load Json/ShortJson_electrical_mechanical_Deformation.json
    {
      "Purpose": "Analytical parameters for electrical potential distribution",
      "Timestamp": "08/12/2014 15:21:01",
      "Muscle": {
        "Kind": "uterine",
        "parameters": [
        ],
        "Models": [
          {
            "name": "Tissue grid",
            "class": "tissular.TissueGridDeformation",
            "parameters": [
                            {"name": "shape","unit": "","value": [125,125 ]},
              {"name": "tmax","unit": "ms","value": 2e3},
              {"name": "stim_list","unit":"vector","value":[{"amp":0.3,"delay":2,"duration":500,"loc":[25,25],"size":[2,2]}}
                                                           ]
              }
            ]
          },
          {
            "name": "Red3",
            "class": "cellular.Red3",
            "parameters": [
            ]
          },
          {
            "name": "H&M",
            "class": "cellular.HaiMurphy",
            "parameters": [
            ]
          }
        ]
      }
    }

Full Json: Electrical
---------------------

Below, a Full version of a .json file simulation where all electrical
model parameters can be set be the user. Only one stimulation has been
set at the cell position [25,25] at 2 ms for 500 ms.

.. code:: python

    # %load Json/FullJson_Electrical.json
    {
      "Purpose": "Analytical parameters for electrical potential distribution",
      "Timestamp": "08/12/2014 15:21:01",
      "Muscle": {
                  "Kind": "uterine",
                  "parameters": [
                  ],
                  "Models": [
                              {
                                "name": "Tissue grid",
                                "class": "tissular.TissueGrid",
                                "parameters": [
                                  {"name": "shape","unit": "","value": [125,125 ]},
                                  {"name": "tmax","unit": "ms","value": 2e3},
                                  {"name": "cellsize_x","unit": "cm","value": 0.05},
                                  {"name": "cellsize_y","unit": "cm","value": 0.05},
                                  {"name": "cellsize_z","unit": "cm","value": 0.05},
                                  {"name": "dt","unit": "ms","value": 0.1},
                                  {"name": "f_sample","unit": "ms","value": 20},
                                  {"name": "resist_x","unit": "","value": 2000},
                                  {"name": "resist_y","unit": "","value": 2000},
                                  {"name": "resist_z","unit": "","value": 2000},
                                  {"name": "stim_list","unit":"vector","value":[{"amp":0.3,"delay":2,"duration":500,"loc":[25,25],"size":[2,2]},
                                                                                {"amp": 0.3,"delay": 600,"duration": 500,"loc": [75,75],"size": [2,2]}
                                                                               ]}
                                ]
                              },
                              {
                                "name": "Red3",
                                "class": "cellular.Red3",
                                "parameters": [
                                                { "name": "Ca0","unit": "","value": 23},
                                                { "name": "Ek","unit": "mV","value": -83},
                                                { "name": "El","unit": "mV","value": -20},
                                                { "name": "F","unit": "","value": 96.487},
                                                { "name": "Gca2","unit": " ","value":0.02694061 },
                                                { "name": "Gk","unit": "","value": 0.064},
                                                { "name": "Gkca","unit": "","value": 0.08},
                                                { "name": "Gl","unit": "","value": 0.0055},
                                                { "name": "Istim","unit": "mA","value": 0.0},
                                                { "name": "Jbase","unit": "","value":0.02397327 },
                                                { "name": "Kca","unit": "","value": 0.01},
                                                { "name": "Kd","unit": "","value": 0.01},
                                                { "name": "R","unit": "","value": 8.314},
                                                { "name": "Rca","unit": "mV","value":5.97139101 },
                                                { "name": "T","unit": "K","value": 295},
                                                { "name": "alpha","unit": "","value":4e-05 },
                                                { "name": "cm","unit": "","value":1 },
                                                { "name": "fc","unit": "","value": 0.4},
                                                { "name": "vca2","unit": "mv","value":-20.07451779 }
                                ]
                              }
                            ]
                }
    }

Full Json: Electrical and Mechanical
------------------------------------

Below, a Full version of a .json file simulation where all electrical
and Mechanical model parameters can be set be the user. Only one
stimulation has been set at the cell position [25,25] at 2 ms for 500
ms.

.. code:: python

    # %load Json/FullJson_Electrical_Mechanical.json
    {
      "Purpose": "Analytical parameters for electrical potential distribution",
      "Timestamp": "08/12/2014 15:21:01",
      "Muscle": {
                  "Kind": "uterine",
                  "parameters": [
                  ],
                  "Models": [
                              {
                                "name": "Tissue grid",
                                "class": "tissular.TissueGrid",
                                "parameters": [
                                  {"name": "shape","unit": "","value": [125,125 ]},
                                  {"name": "tmax","unit": "ms","value": 2e3},
                                  {"name": "cellsize_x","unit": "cm","value": 0.05},
                                  {"name": "cellsize_y","unit": "cm","value": 0.05},
                                  {"name": "cellsize_z","unit": "cm","value": 0.05},
                                  {"name": "dt","unit": "ms","value": 0.1},
                                  {"name": "f_sample","unit": "ms","value": 20},
                                  {"name": "resist_x","unit": "","value": 2000},
                                  {"name": "resist_y","unit": "","value": 2000},
                                  {"name": "resist_z","unit": "","value": 2000},
                                  {"name": "stim_list","unit":"vector","value":[{"amp":0.3,"delay":2,"duration":500,"loc":[25,25],"size":[2,2]},
                                                                                {"amp": 0.3,"delay": 600,"duration": 500,"loc": [75,75],"size": [2,2]}
                                                                               ]}
                                ]
                              },
                              {
                                "name": "Red3",
                                "class": "cellular.Red3",
                                "parameters": [
                                                { "name": "Ca0","unit": "","value": 23},
                                                { "name": "Ek","unit": "mV","value": -83},
                                                { "name": "El","unit": "mV","value": -20},
                                                { "name": "F","unit": "","value": 96.487},
                                                { "name": "Gca2","unit": " ","value":0.02694061 },
                                                { "name": "Gk","unit": "","value": 0.064},
                                                { "name": "Gkca","unit": "","value": 0.08},
                                                { "name": "Gl","unit": "","value": 0.0055},
                                                { "name": "Istim","unit": "mA","value": 0.0},
                                                { "name": "Jbase","unit": "","value":0.02397327 },
                                                { "name": "Kca","unit": "","value": 0.01},
                                                { "name": "Kd","unit": "","value": 0.01},
                                                { "name": "R","unit": "","value": 8.314},
                                                { "name": "Rca","unit": "mV","value":5.97139101 },
                                                { "name": "T","unit": "K","value": 295},
                                                { "name": "alpha","unit": "","value":4e-05 },
                                                { "name": "cm","unit": "","value":1 },
                                                { "name": "fc","unit": "","value": 0.4},
                                                { "name": "vca2","unit": "mv","value":-20.07451779 }
                                ]
                              },
                              {
                                "name": "H&M",
                                "class": "cellular.HaiMurphy",
                                "parameters": [
                                                { "name": "Ca05MLCK","unit": "","value":0.001 },
                                                { "name": "K","unit": "","value": 1},
                                                { "name": "K2","unit": "","value": 0.0001399},
                                                { "name": "K3","unit": "","value": 0.0144496},
                                                { "name": "K4","unit": "","value":0.0036124 },
                                                { "name": "K7","unit": "","value": 0.000134},
                                                { "name": "nM","unit": "","value": 4.7135}
                                ]
                              }
                            ]
                }
    }

Full Json: Electrical, Mechanical and Deformation
-------------------------------------------------

Below, a Full version of a .json file simulation where all electrical,
Mechanical and Deformation model parameters can be set be the user. Only
one stimulation has been set at the cell position [25,25] at 2 ms for
500 ms.

.. code:: python

    # %load Json/FullJson_Electrical_Mechanical_Deformation.json
    {
      "Purpose": "Analytical parameters for electrical potential distribution",
      "Timestamp": "08/12/2014 15:21:01",
      "Muscle": {
                  "Kind": "uterine",
                  "parameters": [
                  ],
                  "Models": [
                              {
                                "name": "Tissue grid",
                                "class": "tissular.TissueGridDeformation",
                                "parameters": [
                                  {"name": "shape","unit": "","value": [125,125 ]},
                                  {"name": "tmax","unit": "ms","value": 2e3},
                                  {"name": "cellsize_x","unit": "cm","value": 0.05},
                                  {"name": "cellsize_y","unit": "cm","value": 0.05},
                                  {"name": "cellsize_z","unit": "cm","value": 0.05},
                                  {"name": "dt","unit": "ms","value": 0.1},
                                  {"name": "f_sample","unit": "ms","value": 20},
                                  {"name": "resist_x","unit": "","value": 2000},
                                  {"name": "resist_y","unit": "","value": 2000},
                                  {"name": "resist_z","unit": "","value": 2000},
                                  {"name": "stim_list","unit":"vector","value":[{"amp":0.3,"delay":2,"duration":500,"loc":[25,25],"size":[2,2]},
                                                                                {"amp": 0.3,"delay": 600,"duration": 500,"loc": [75,75],"size": [2,2]}
                                                                               ]}
                                ]
                              },
                              {
                                "name": "Red3",
                                "class": "cellular.Red3",
                                "parameters": [
                                                { "name": "Ca0","unit": "","value": 23},
                                                { "name": "Ek","unit": "mV","value": -83},
                                                { "name": "El","unit": "mV","value": -20},
                                                { "name": "F","unit": "","value": 96.487},
                                                { "name": "Gca2","unit": " ","value":0.02694061 },
                                                { "name": "Gk","unit": "","value": 0.064},
                                                { "name": "Gkca","unit": "","value": 0.08},
                                                { "name": "Gl","unit": "","value": 0.0055},
                                                { "name": "Istim","unit": "mA","value": 0.0},
                                                { "name": "Jbase","unit": "","value":0.02397327 },
                                                { "name": "Kca","unit": "","value": 0.01},
                                                { "name": "Kd","unit": "","value": 0.01},
                                                { "name": "R","unit": "","value": 8.314},
                                                { "name": "Rca","unit": "mV","value":5.97139101 },
                                                { "name": "T","unit": "K","value": 295},
                                                { "name": "alpha","unit": "","value":4e-05 },
                                                { "name": "cm","unit": "","value":1 },
                                                { "name": "fc","unit": "","value": 0.4},
                                                { "name": "vca2","unit": "mv","value":-20.07451779 }
                                ]
                              },
                              {
                                "name": "H&M",
                                "class": "cellular.HaiMurphy",
                                "parameters": [
                                                { "name": "Ca05MLCK","unit": "","value":0.001 },
                                                { "name": "K","unit": "","value": 1},
                                                { "name": "K2","unit": "","value": 0.0001399},
                                                { "name": "K3","unit": "","value": 0.0144496},
                                                { "name": "K4","unit": "","value":0.0036124 },
                                                { "name": "K7","unit": "","value": 0.000134},
                                                { "name": "nM","unit": "","value": 4.7135}
                                ]
                              }
                            ]
                }
    }

