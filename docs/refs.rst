References
==========

.. [maggio2012modified] Maggio, C. D., Jennings, S. R., Robichaux, J. L., Stapor, P. C., & Hyman, J. M. (2012). **A modified Hai�Murphy model of uterine smooth muscle contraction.** *Bulletin of mathematical biology, 74(1), 143-158.*
.. [laforet2011toward] Laforet, J., Rabotti, C., Terrien, J., Mischi, M., & Marque, C. (2011). **Toward a multiscale model of the uterine electrical activity.** *Biomedical Engineering, IEEE Transactions on, 58(12), 3487-3490.*
.. [beck2010worldwide]
.. [huddy2001educational]
.. [devedeux1993uterine]
.. [rabotti2008estimation]
.. [young2007myocytes]
.. [koenigsberger20042]
.. [hai1988cross]
.. [bursztyn2007mathematical]
.. [ozkaya2012mechanical]
